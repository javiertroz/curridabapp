angular.module('starter.controllers', [])

.controller('DashCtrl', function($scope) {})

.controller('ChatsCtrl', function($scope, Chats) {
  // With the new view caching in Ionic, Controllers are only called
  // when they are recreated or on app start, instead of every page change.
  // To listen for when this page is active (for example, to refresh data),
  // listen for the $ionicView.enter event:
  //
  //$scope.$on('$ionicView.enter', function(e) {
  //});

  $scope.chats = Chats.all();
  $scope.remove = function(chat) {
    Chats.remove(chat);
  };
})

.controller('ChatDetailCtrl', function($scope, $stateParams, Chats) {
  $scope.chat = Chats.get($stateParams.chatId);
})

.controller('AccountCtrl', function($scope) {
  $scope.settings = {
    enableFriends: true
  };
})

.controller('LugaresCtrl', ['$scope', 'Lugares', '$state', '$stateParams', function($scope, Lugares, $state, $stateParams) {
  Lugares.success(function(data) {
    $scope.lugares = data;

  });
}])

.controller('LugarDetailCtrl', ['$scope', '$state', '$stateParams', 'Lugares', function($scope, $state, $stateParams, Lugares) {
  Lugares.success(function(data) {
      for(var i = 0;  i < data.length; i++){
        var item = data[i];
        if(item.title == $stateParams.lugarId){
            $scope.selectedPlace = item;
            $scope.areaServicio = item.areaServicio;
            break;
        }
      }
  });
}])

.controller('MunicipalidadCtrl', ['$scope', '$state', '$stateParams', 'Lugares', function($scope, $state, $stateParams, Lugares) {
  Lugares.success(function(data) {
      $scope.municipalidad = data[0];
      $scope.areaServicio = data[0].areaServicio;
      $scope.funcionarios = data[0].areaServicio[0].funcionarios;
  });
}])

.controller('InicioController', ['$scope', '$http', '$state',
    function($scope, $http, $state) {
    $http.get('js/info.json')
    .success(function(data) {
      $scope.lugares = data.place;
      //$scope.wichperson = $state.params.id;
      for(var i = 0;  i < data.place.length; i++){
        var item = data.place[i]; 
        for(var j = 0; j < item.funcionarios.length; j++){
          var person = item.funcionarios[j];
          if(person.pId == $state.params.id){
            $scope.selectedPerson = person;
            $scope.wichperson = $state.params.id;
            console.log(person);
            break;
        }
        }
      }

      $scope.onItemDelete = function(dayIndex, item) {
        $scope.lugares[dayIndex].funcionarios.splice($scope.lugares[dayIndex].funcionarios.indexOf(item), 1);
      }

      $scope.doRefresh =function() {
      $http.get('js/info.json').success(function(data) {
          $scope.lugares = data.place;
          $scope.$broadcast('scroll.refreshComplete');
        });
      }

      $scope.toggleStar = function(item) {
        item.star = !item.star;
      }

    })
    .error(function(err){
      return err;
    });
}])